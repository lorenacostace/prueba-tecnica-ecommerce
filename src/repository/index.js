import { setup } from 'axios-cache-adapter'
import localforage from 'localforage'
import memoryDriver from 'localforage-memoryStorageDriver'
import { configApi } from '../config/api'
import {
    getProducts,
    getProductById
} from './product'
import { postCart } from "./cart";

const {
    endpoint,
    baseUrl,
    apiVersion
} = configApi;

async function configure () {
    await localforage.defineDriver(memoryDriver)
    const forageStore = localforage.createInstance({
        driver: [
            localforage.LOCALSTORAGE,
            localforage.INDEXEDDB,
            memoryDriver._driver
        ],
        name: 'cache'
    })
    return setup({
        baseURL: baseUrl + apiVersion,
        timeout: 7000,
        cache: {
            maxAge: 60 * 60 * 1000,
            store: forageStore,
            exclude: { query: false },
            key: req => req.url + JSON.stringify(req.params),
        }
    })
}

export const getCartStorage = () => {
    const item = localStorage.getItem('cartCounter');

    if(!item)
        return null;
    const cart = JSON.parse(item);
    const currentDate = new Date();

    if(currentDate > cart.expires) {
        localStorage.removeItem('cartCounter');
        return null;
    }
    return cart.value;
}

 export const setCartStorage = (value) => {
    const currentDate = new Date();

    const cart = {
        value,
        expires: currentDate.getTime() + 60 * 60 * 1000,
    }

    localStorage.setItem('cartCounter', JSON.stringify(cart));
}

export const productRepository = {
    getProducts: () => configure().then(api => getProducts({ api, endpoint: endpoint.product })),
    getProductById: (id) => configure().then(api => getProductById({ api, endpoint: endpoint.product, id}))
}

export const cartRepository = {
    postCart: (body) => configure().then(api => postCart({ api, endpoint: endpoint.cart, body}))
}
