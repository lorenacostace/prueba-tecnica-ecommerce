import styled from "styled-components";

export const Page = styled.div`
  width: 100%;
  min-height: 100vh;
`;

export const Header = styled.header`
  position: relative;
  top: 0;
  width: 100%;
  z-index: 20;
`;