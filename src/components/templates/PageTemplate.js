import React from "react";
import PropTypes from 'prop-types'
import { Page, Header } from "./PageTemplateStyle";


const PageTemplate = ({
                          header, children, ...props
                      }) => (
    <Page {...props}>
        {header && <Header>{ header }</Header>}
        {children}
    </Page>
);

PageTemplate.propTypes = {
    children: PropTypes.any.isRequired,
    header: PropTypes.node,
};

PageTemplate.defaultProps = {
    header: null,
};

export default PageTemplate;
