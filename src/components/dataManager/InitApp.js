import React from 'react';
import { getCartStorage, setCartStorage } from "../../repository";
import { actions as actionsCart } from "../../redux/cart";
import { connect } from "react-redux";

const mapDispatchToProps = (dispatch) => {
    return {
        updateCart: (cart) => dispatch(actionsCart.setCart(cart)),
    }
}

class InitApp extends React.Component {
    componentDidMount() {
        this.initCart()
    }

    initCart() {
        const cart = getCartStorage();
        if(cart === null){
            setCartStorage(0);
            this.props.updateCart(0);
        } else {
            this.props.updateCart(cart);

        }
    }
    render() {
        return null
    }
}

const InitAppContainer = connect(
    null,
    mapDispatchToProps
)(InitApp)

export default InitAppContainer;
