import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import store from '../src/redux/store'
import routes from "./config/routes";
import '../src/normalize.css';
import { BaseCSS } from "styled-bootstrap-grid";
import InitApp from "./components/dataManager/InitApp";

ReactDOM.render(
    <Provider store={ store }>
        <React.StrictMode>
            <InitApp/>
            <BaseCSS/>
            <Router>
                <Switch>
                    { routes.map(({ path, Component }, key) => (
                        <Route exact path={ path } key={ key } component={ Component } />
                    ))}
                </Switch>
            </Router>
        </React.StrictMode>
    </Provider>,
  document.getElementById('root')
);

